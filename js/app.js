// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getDatabase,onValue,ref,set,child,get,update,remove } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";
import { getStorage,ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD3qU_QBnPpoVTxMhsSejna1gD11y4FM14",
  authDomain: "proyecto-53998.firebaseapp.com",
  databaseURL: "https://proyecto-53998-default-rtdb.firebaseio.com/",
  projectId: "proyecto-53998",
  storageBucket: "proyecto-53998.appspot.com",
  messagingSenderId: "667802560467",
  appId: "1:667802560467:web:9bd871b8a6f3b623d2f126",
  measurementId: "G-DJ92MQ6RQZ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const auth = getAuth();
const storage = getStorage();

// Botones
var btnLoguear = document.getElementById('btnLogin');
var btnDisconnect = document.getElementById('btnDesconectar');

var email = "";
var password = "";

function leer2(){
    email = document.getElementById('correo').value;
    password = document.getElementById('pass').value;
}

// Permite ver el estado de la sesión del usuario
function comprobarAUTH(){
    
    onAuthStateChanged(auth, (user) => {
      if (user) {
        //alert("Usuario detectado");
        // ...
      } else {
        // User is signed out
        // ...
        //alert("No se ha detectado un usuario");
        window.location.href="/html/index.html";
      }
    });
  }


if(window.location.href == "https://proyectojair.000webhostapp.com/html/admin.html"){
    window.onload = comprobarAUTH();
}

if(btnLoguear){
    btnLoguear.addEventListener('click', (e)=>{
    e.preventDefault();
    leer2();
    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        alert("Ha iniciado sesión")
        window.location.href="/html/admin.html";
        // ...
    })
    .catch((error) => {
        alert("Ingrese de nuevo los datos")
        const errorCode = error.code;
        const errorMessage = error.message;
    });
    });
}

if(btnDisconnect){
    btnDisconnect.addEventListener('click',  (e)=>{
    e.preventDefault();
    signOut(auth).then(() => {
    alert("Ha cerrado sesión");
    window.location.href="/html/index.html";
    // Sign-out successful.
    }).catch((error) => {
    // An error happened.
    });
    });
}

// Panel de administración

// Declaración de variables
var btnInsertar = document.getElementById("btnAgregar");
var btnConsultar = document.getElementById("btnConsultar");
var btnActualizar = document.getElementById("btnActualizar");
var btnDeshabilitar = document.getElementById("btnDeshabilitar");
var btnLimpiar = document.getElementById("btnLimpiar");
var btnMostrar = document.getElementById("btnMostrarRegistros");
var archivo = document.getElementById('archivo');

var codigo = "";
var precio = "";
var nombre = "";
var descripcion = "";
var urlImagen = "";
var estatus = 0;
var nombreImagen = "";

function insertar() {
    event.preventDefault();
    leer();
    const dbref = ref(db);
    if(nombre=="" || descripcion == "" || codigo =="" || precio =="" || urlImagen=="" || estatus==""){
        alert("Complete los campos");
    }else{
        //Validación para que no se repita el producto
        get(child(dbref, "productos/" + codigo))
        .then((snapshot) => {
        if (snapshot.exists() == true) {
            alert('El producto ya existe');
            return;
        }
        set(ref(db,"productos/" + codigo),{
            nombre:nombre,
            precio:precio,
            descripcion:descripcion,
            estatus:estatus,
            urlImagen:urlImagen})
            .then(() => {
                alert("Producto añadido");
                mostrarProductos();
            })
            .catch((error) => {
                alert("No se pudo insertar el producto -> " + error);
            });  
        })
        .catch((error) => {
            alert("Ocurrio un error " + error);
        });
        
    }
}

function actualizar() {
    event.preventDefault();
    leer();
    if(nombre=="" || descripcion == "" || codigo =="" || precio =="" || urlImagen=="" || estatus==""){
        alert("Complete los campos");
    }else {
        update(ref(db, "productos/" + codigo), {
        nombre:nombre,
        precio:precio,
        descripcion:descripcion,
        estatus:estatus,
        urlImagen:urlImagen,
    })
        .then(() => {
            alert("El registro se actualizó");
        mostrarProductos();
        })
        .catch((error) => {
            alert("No se pudo actualizar -> " + error);
        });
    }
}

function mostrarProducto() {
    event.preventDefault();
    leer();
    if(codigo==""){
        alert('Ingresa un código');
        return;
    }
    const dbref = ref(db);
    get(child(dbref, "productos/" + codigo))
        .then((snapshot) => {
        if (snapshot.exists()) {
            nombre = snapshot.val().nombre;
            precio = snapshot.val().precio;
            descripcion = snapshot.val().descripcion;
            estatus = snapshot.val().estatus;
            urlImagen = snapshot.val().urlImagen;
            escribirInputs();
        } else {
            alert("No existe el producto");
        }
    })
    .catch((error) => {
        alert("Surgio un error " + error);
    });
}

function deshabilitar() {
    event.preventDefault();
    leer();
    // Referencia a la base de datos
    if(codigo =="" ){
        alert("Introduce un código");
    }else{
        update(ref(db, "productos/" + codigo), {
        estatus:1
    })
    .then(() => {
        alert("El Producto ha sido deshabilitado");
        mostrarProductos();
        })
        .catch((error) => {
        alert("No se pudo actualizar -> " + error);
        });
    }
}

async function cargarImagen(){
    const file = event.target.files[0];
    const name = event.target.files[0].name;
    
    const storageRef = refStorage(storage, 'imagenes/'+name);
    await uploadBytes(storageRef, file).then((snapshot) => {
    nombreImagen = name;s
    });

}

function descargarImagen(){

    archivo = nombreImagen;
    alert('Archivo = '+ archivo);
    const storageRef = refStorage(storage, 'imagenes/'+ archivo);
    // Get the download URL
    getDownloadURL(storageRef)
    .then((url) => {
        document.getElementById('url').value=url;
        urlImagen =url;
        document.getElementById('imgPreview').src=urlImagen;
        document.getElementById('imgPreview').classList.remove('none');
    })
    .catch((error) => {
    
    switch (error.code) {
        case 'storage/object-not-found':
            alert("No existe el archivo");
        break;

        case 'storage/unauthorized':
            alert("No tiene permisos");
        break;

        case 'storage/canceled':
            alert("Se canceló la subida")
        break;

        case 'storage/unknown':
            alert("Error desconocido");
        break;
    }
    });
}

async function obtenerUrl(){
    await cargarImagen();
    await descargarImagen();
}

var marcoProductos = document.getElementById('marcoProductos');

if(window.location.href == "https://proyectojair.000webhostapp.com/html/productos.html"){
    
    mostrarLProductos();
    function mostrarLProductos(){

        const dbRef = ref(db, "productos");
        
        onValue(dbRef,(snapshot) => {
    
            snapshot.forEach((childSnapshot) => {
                const childKey = childSnapshot.key;
                const childData = childSnapshot.val();
                
                
                if(marcoProductos){
                    
                    if(childData.estatus == 0){
                        marcoProductos.innerHTML = marcoProductos.innerHTML +  `<div class="imagenes">  <div class="producto"> 
                            <picture> <img class="imgProducto" src="${childData.urlImagen}" alt="PRODUCTO"></picture> <br>
                            <p class="txtProd">${childData.nombre}</p>
                            <br>
                            <p class="txtProd">${childData.precio}</p>
                            <br>
                            <p class="txtProd">${childData.descripcion}</p>
                        </div>
                    </div>`;
                    }
                }
                
            });
        });
        
    }
    
}



function mostrarProductos(){
    //event.preventDefault();
    const dbRef = ref(db, "productos");
    productos.classList.remove("d-none");

    productos.innerHTML = `<thead><tr>
					<th scope="col" width="5%" class="text-center">Código</th>
					<th scope="col" width="30%" class="text-center">Nombre</th>
					<th scope="col" width="15%" class="text-center">Precio</th>
					<th scope="col" width="20%" class="text-center">Descripcion</th>
					<th scope="col" width="15%" class="text-center">Imagen</th>
                    <th scope="col" width="15%" class="text-center">estatus</th>
				</tr></thead><tbody></tbody>`;
onValue(dbRef,(snapshot) => {

    snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

            productos.lastElementChild.innerHTML += `<tr>
						<th class="text-center" scope="row">${childKey}</th>
						<td class="text-center">${childData.nombre}</td>
						<td class="text-center">${childData.precio}</td>
						<td class="text-center">${childData.descripcion}</td>
						<td class="text-center p-0"><img class="w-100" src="${childData.urlImagen}" alt="Imagen de ${childData.nombre}"/></td>
            <td class="text-center">${childData.estatus}</td>
					</tr>`;
    });
    },
    {
        onlyOnce: true,
    }
    
    );
}

function leer(){
    precio = document.getElementById("precio").value;
    nombre = document.getElementById("nombre").value;
    descripcion = document.getElementById("descripcion").value;
    codigo = document.getElementById('codigo').value;
    urlImagen = document.getElementById('url').value;
    estatus = document.getElementById('estatus').value;
}

function escribirInputs(){
    document.getElementById('precio').value = precio;
    document.getElementById('nombre').value = nombre;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('codigo').value = codigo;
    document.getElementById('url').value = urlImagen;
    document.getElementById('imgPreview').src=document.getElementById('url').value;
    document.getElementById('imgPreview').classList.remove('none');
    document.getElementById('estatus').value=estatus;
}

function limpiar(){
    event.preventDefault();
    document.getElementById("precio").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("descripcion").value = "";
    document.getElementById('codigo').value = "";
    document.getElementById('url').value = "";
    document.getElementById('imgPreview').src =""; 
    document.getElementById('estatus').value="";
    document.getElementById('imgPreview').classList.add('none');
}

btnInsertar.addEventListener("click", insertar);
archivo.addEventListener("change", obtenerUrl);
btnConsultar.addEventListener('click', mostrarProducto);
btnLimpiar.addEventListener('click', limpiar);
btnDeshabilitar.addEventListener('click', deshabilitar);
btnActualizar.addEventListener('click',actualizar);
btnMostrar.addEventListener('click', mostrarProductos);
